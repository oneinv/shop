<?php
    require_once 'con_to_db.php';
    require_once 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php include ("header.php") ?>
	<h3> Welcome, dear Admin <?php echo $_SESSION['logged_user']; ?> !</h3><br>
    <?php $num = is_admin($con, $_SESSION['logged_user']);?>
    <a href="item_add.php">Create item</a>
    <br />
    <a href="item_edit.php">Change item</a>
    <br />
    <a href="item_delete.php">Delete item</a>
    <br />
    <a href="categ_add.php">Create category</a>
    <br />
    <a href="category_edit.php">Change category</a>
    <br />
    <a href="category_delete.php">Delete category</a>
    <br />
    <a href="user_add.php">Create user</a>
    <br />
    <a href="user_edit.php">Change user</a>
    <br />
    <a href="user_delete.php">Delete user</a>
    <br />
    <a href="../index.php">Main page</a>
</body>
</html>
