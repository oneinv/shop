<?php
	require_once('../con_to_db.php');
	require_once('../functions.php');

$orders = get_orders($con);
?>
<html>
	<body>
		<?php foreach ($orders as $order): ?>
			<div>
				<?php $order['products'] = json_decode($order['products'], true);?>
				<p><?=$order['id'];?></p>
				<p><?=$order['name'];?></p>
				<p><?=$order['phone'];?></p>
				<p><?=$order['comment'];?></p>
				<?php $products = $order['products'];?>
				<?php foreach ($products as $prod):?>
					<p><?php echo $prod['name'];?></p>
					<p><?php echo $prod['price'];?></p>
				<?php endforeach;?>
			</div>
		<?php endforeach;?>

	</body>
	</html>
