<?php
	require_once 'con_to_db.php';
	require_once 'functions.php';

?>
<!DOCTYPE html>
<html>
<head>
	<title>Basket</title>
</head>
<body>
	<?php include ("header.php") ?>
	<?php
	require_once 'functions.php';
	$cart_total = 0;
	$name = $_SESSION['logged_user'];
	$var = implode(',', $_SESSION['cart']);
	$busket = get_items($con, $var);
	foreach ($busket as $busk) {
		$cart_total += $busk['price'];
	}

    if ($_POST['submit'] == "Make order") {
		$var = implode(',', $_SESSION['cart']);
		$busket = get_items($con, $var);
		$phone = get_phone($con, $name);
		$comm = htmlspecialchars($_POST['comm']);
		$home = get_home($con, $name);
		$products = json_encode($busket, JSON_UNESCAPED_UNICODE);
		$id = get_id($con, $name);
		if ($busket != 0) {
			orderAdd($con, $name, $phone, $comm, $home, $id, $products, 0);
	        echo "<h1 class='good'>Order placed</h1>";
	        $_SESSION['cart'] = array();
		} else {
			echo "<h1> please change goods </h1>";
		}
    }

	if ($_POST['submit'] == "Empty busket" && isset($_SESSION['cart']))
	{
		$_SESSION['cart'] = array();
	}

	$var = implode(',', $_SESSION['cart']);
	$busket = get_items($con, $var);
	$cart_total = 0;
	?>
	<?php foreach ($busket as $cart): ?>
			<div><p><?=$cart["name"]?></p></div>
			<?php $cart_total = $cart_total + $cart["price"]?>
	<?php endforeach; ?>
	<?php if ($cart_total == 0)
			echo "<h1 class='good'>EMPTY</h1>";
		else {
		 echo "<h1>PRICE: <h1>";
		 echo $cart_total; }?>
	<form action="basket.php" method="POST">
			<input type="submit" name="submit" value="Empty busket" />
			<input type="text" name="comm" value="" />
			<input type="submit" name="submit" value="Make order" />
		</form>
</body>
</html>
